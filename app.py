from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from celery import Celery
import os


base_dir = os.path.dirname(os.path.realpath(__file__))
db_file_path = os.path.join(base_dir, "demo.db")
db_path = 'sqlite:///'+db_file_path
print(db_path)

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = db_path
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'


db = SQLAlchemy(app)
ma = Marshmallow(app)

from model import *

celery_app = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery_app.conf.update(app.config)

from celery_scheduled import *

from controller import *

api = Api(app)
from view import *


if __name__ == '__main__':
    app.run(debug=True)