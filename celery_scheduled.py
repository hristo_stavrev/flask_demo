import requests
import time
from app import celery_app
from model import Contact


@celery_app.task
def post_new_random_contact(length):
    print("post_new_random_contact task")
    r = requests.post('http://localhost:5000/contacts', json=Contact.create_random(length))
    print(r.json())


@celery_app.task
def delete_old_contacts(age_in_seconds):
    print("delete_old_contacts task")
    r = requests.get('http://localhost:5000/contacts', headers={'Connection':'close'})
    contacts_objects = r.json()
    age_threshold_time = int(time.time() - age_in_seconds)
    print(age_threshold_time)
    print(age_in_seconds)
    print(time.time())
    print(contacts_objects)

    older_contacts = list(filter(lambda x: x['last_update_timestamp'] < age_threshold_time, contacts_objects))

    print("older_contacts" + str(list(older_contacts)))
    for old_contact in older_contacts:
        del_url = 'http://localhost:5000/contacts/' + old_contact['username']
        print(del_url)
        del_req = requests.delete(del_url, headers={'Connection':'close'})
        print(del_req)


celery_app.add_periodic_task(15.0, post_new_random_contact.s(5), name='add new every 15')
celery_app.add_periodic_task(60.0, delete_old_contacts.s(60), name='delete old')