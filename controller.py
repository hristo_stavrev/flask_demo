from app import db
from model import *
from flask_restful import Resource
from flask import request
import time


class ContactListResource(Resource):
    def get(self):
        contacts = Contact.query.all()
        return contacts_schema.dump(contacts)

    def post(self):
        new_contact = contact_schema.load(request.json)
        new_contact.last_update_timestamp = time.time()
        print(new_contact)
        db.session.add(new_contact)

        if 'emails' in request.json:
            loaded_emails = emails_schema.load(request.json['emails'])
            for e in loaded_emails:
                e.owner_username = new_contact.username
                db.session.add(e)

        db.session.commit()
        return contact_schema.dump(new_contact)


class ContactResource(Resource):
    def get(self, username):
        contact = Contact.query.get_or_404(username)
        return contact_schema.dump(contact)

    def patch(self, username):
        contact = Contact.query.get_or_404(username)
        contact.last_update_timestamp = time.time()

        if 'first_name' in request.json:
            contact.first_name = request.json['first_name']
        if 'last_name' in request.json:
            contact.last_name = request.json['last_name']
        if 'emails' in request.json:
            loaded_emails = emails_schema.load(request.json['emails'])
            email_addresses_incoming = list(map(lambda x: x.email, loaded_emails))
            for c_email in contact.emails:
                if c_email.email not in email_addresses_incoming:
                    db.session.delete(c_email)

            email_addresses_registered = list(map(lambda x: x.email, contact.emails))
            for e in loaded_emails:
                if e.email not in email_addresses_registered:
                    e.owner_username = contact.username
                    db.session.add(e)

        db.session.commit()
        return contact_schema.dump(contact)

    def delete(self, username):
        contact = Contact.query.get_or_404(username)
        print("Deleting contact " + contact.__repr__())
        db.session.query(Email).filter(Email.owner_username == contact.username).delete()
        db.session.commit()
        db.session.delete(contact)
        db.session.commit()
        return 'deleted ' + username, 204


class AllEmailListResource(Resource):
    def get(self):
        emails = Email.query.all()
        return emails_schema.dump(emails)


class EmailListResource(Resource):
    def get(self, username):
        contact = Contact.query.get_or_404(username)
        emails = contact.emails
        return emails_schema.dump(emails)

    def post(self, username):
        contact = Contact.query.get_or_404(username)
        incoming_email = email_schema.load(request.json)
        incoming_email.owner_username = contact.username
        db.session.add(incoming_email)
        db.session.commit()
        emails = contact.emails
        return emails_schema.dump(emails)


class EmailCRUDResource(Resource):
    def get(self, username, email):
        email_object = Email.query.get_or_404(email)

        # make sure the email belongs to the correct user
        contact_object = Contact.query.get_or_404(username)
        assert contact_object.username == email_object.owner_username

        return email_schema.dump(email_object)

    def delete(self, username, email):
        email_object = Email.query.get_or_404(email)

        # make sure the email belongs to the correct user
        contact_object = Contact.query.get_or_404(username)
        assert contact_object.username == email_object.owner_username

        db.session.delete(email_object)
        db.session.commit()
        return email_schema.dump(email_object)
