from app import db
from app import ma
import random
import string


class Contact(db.Model):
    username = db.Column(db.String(255), primary_key=True)
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    last_update_timestamp = db.Column(db.BigInteger())

    def __repr__(self):
        return '<Contact %s >' % self.username

    def create_random(string_length):
        letters = string.ascii_lowercase
        first_name = ''.join(random.choice(letters) for i in range(string_length))
        last_name = ''.join(random.choice(letters) for i in range(string_length))
        email = first_name + "_" + last_name +"@yahoo.com"
        return {
            "username" : first_name,
            "first_name":first_name,
            "last_name": last_name,
            "emails":[{"email":email}],
        }


class Email(db.Model):
    email = db.Column(db.String(255), primary_key=True)
    owner_username = db.Column(db.Integer, db.ForeignKey("contact.username"))
    owner = db.relationship("Contact", backref="emails")

    def __repr__(self):
        return '<Email %s associated to %s >' % (self.email, self.owner_username)


class ContactSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Contact
        load_instance = True

    username = ma.auto_field()
    first_name = ma.auto_field()
    last_name = ma.auto_field()
    last_update_timestamp = ma.auto_field()
    emails = ma.auto_field()


class EmailSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Email
        include_fk = True
        load_instance = True

    email = ma.Email()


contact_schema = ContactSchema()
contacts_schema = ContactSchema(many=True)
email_schema = EmailSchema()
emails_schema = EmailSchema(many=True)
