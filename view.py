from app import api
from controller import *

api.add_resource(ContactListResource, '/contacts')
api.add_resource(ContactResource, '/contacts/<string:username>')
api.add_resource(AllEmailListResource, '/emails')
api.add_resource(EmailListResource, '/contacts/<string:username>/emails')
api.add_resource(EmailCRUDResource, '/contacts/<string:username>/emails/<string:email>')